import { List } from "@material-ui/core";
import React from "react";
import Todo from "./Todo";

export default function TodoList({ todos, toggleComplete, removeTodo }) {
  return (
    <List className="todo-list">
      {todos.map((todo, index) => (
        <Todo
          key={todo.id}
          todo={todo}
          toggleComplete={toggleComplete}
          removeTodo={removeTodo}
        />
      ))}
    </List>
  );
}
