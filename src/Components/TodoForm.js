import { Button, TextField } from "@material-ui/core";
import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";

//required from parent component ->
// param : addTodo
export default function TodoForm({ addTodo }) {
  const [todo, setTodo] = useState({
    id: "",
    task: "",
    completed: false,
  });

  // to update the task property in todo
  function handleTaskInputChange(e) {
    setTodo({ ...todo, task: e.target.value });
  }

  function handleSubmit(e) {
    e.preventDefault(); // to prevent from default handle submit action
    // to make sure that we are not submitting empty values
    if (todo.task && todo.task.trim) {
      addTodo({ ...todo, id: uuidv4() });

      setTodo({ ...todo, task: "" }); // resets the form
    }
  }
  return (
    <form onSubmit={handleSubmit} className="todo-form">
      <TextField
        label="Task"
        name="task"
        type="text"
        value={todo.task}
        onChange={handleTaskInputChange}
      />
      <Button type="submit" variant="contained">
        Submit
      </Button>
    </form>
  );
}
