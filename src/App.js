import React, { useState, useEffect } from "react";
import TodoForm from "./Components/TodoForm";
import TodoList from "./Components/TodoList";
import "./App.css";
import { Typography } from "@material-ui/core";

const LOCAL_STORAGE_KEY = "react-todo-list-todos";

function App() {
  const [todos, setTodos] = useState([]);

  // to get data before starting the app
  useEffect(() => {
    const storageTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    if (storageTodos) {
      setTodos(storageTodos);
    }
  }, []);

  //updating the local storage as the
  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos)); // adding to our custom storage key
  }, [todos]); //any changes in todos will trigger useEffect otherwise it is triggerd only when the app starts

  function addTodos(todo) {
    setTodos([todo, ...todos]); // adding new todo in the begining followed by old todos.
  }

  function toggleCompleted(id) {
    setTodos(
      todos.map((todo) => {
        if (todo.id === id) {
          return {
            ...todo,
            completed: !todo.completed,
          };
        }
        return todo;
      })
    );
  }

  function removeTodo(id) {
    setTodos(todos.filter((todo) => todo.id !== id));
  }

  return (
    <div className="App">
      <header className="App-header">
        <Typography variant="h2">React Todo list</Typography>
        <TodoForm addTodo={addTodos} />
        <TodoList
          todos={todos}
          toggleComplete={toggleCompleted}
          removeTodo={removeTodo}
        />
      </header>
    </div>
  );
}

export default App;
